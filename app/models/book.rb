class Book < ApplicationRecord
  has_many :user_shelves
  has_many :users, through: :user_shelves

  validates :isbn, uniqueness: true, presence: true
end
