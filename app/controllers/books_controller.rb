class BooksController < ApplicationController
  before_action :authenticate_user!

  def index
    @books = current_user.books
  end

  def search
    @books = BooksFinder.new(params[:option], params[:term]).find
  end

  def show
    @book = Book.find(params[:id])
  end

  def create
    book = GoogleBooks.search(params[:isbn]).first
    current_user.books.create(
      title: book.title,
      isbn: book.isbn,
      image_link: book.image_link
    )
  end
end