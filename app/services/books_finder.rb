class BooksFinder
  attr_reader :target, :term

  def initialize(target, term)
    @target = target
    @term = term
  end

  def find
    if target == 'user'
      user = User.find_by(username: term)
      user.books
    else
      GoogleBooks.search(term, count: 12)
    end
  end
end