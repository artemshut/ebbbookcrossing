Rails.application.routes.draw do
  devise_for :users

  root to: 'books#search'

  resources :users do
    resources :books do
      collection do
        get 'search'
      end
    end
  end
end
